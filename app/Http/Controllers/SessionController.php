<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Session;

class SessionController extends Controller
{
    public $main_model;

    public function __construct(Session $main_model)
    {
        $this->main_model   = $main_model;
    }
    /**
     * Return whole list of session
     * No authorization required
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $sessions = $this->main_model->get();

        return Response()->json($sessions, 200);

    }

    /**
     * Get detail session
     * No authorization required
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $data = $this->main_model->find($id);

        if(is_null($data)) {
            return Response()->json(
                ['message' => 'Not found!'
            ], 404);
        }

        return Response()->json($data, 200);
    }

     /**
     * Create new session
     * Authorization required
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), $this->main_model->rules());
        if($validator->fails()) {
            return Response()->json(
                ['message' => $validator->errors()->all()
            ], 500);
        }

        $data = $this->main_model->create($request->all());

        if(!$data) {
            return Response()->json(
                ['message' => 'Error saving!'
            ], 500);
        }

        return Response()->json(
            ['message' => 'Store Successfully!'
        ], 201);
    }

    /**
     * Update session
     * Authorization required
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), $this->main_model->rules($id));
        if($validator->fails()) {
            return Response()->json(
                ['message' => $validator->errors()->all()
            ], 500);
        }

        $data = $this->main_model->findOrFail($id);

        if(!$data) {
            return Response()->json([
                "message" => "Error update!"
            ], 500);
        }

        $this->main_model->where('id', $id)
            ->update([
                'name' => $request->name,
                'description' => $request->description,
                'start' => $request->start,
                'duration' => $request->duration,
                'start' => $request->start,
                'duration' => $request->duration
        ]);

        return response()->json($data, 200);
    }

    /**
     * Delete session
     * Authorization required
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id)
    {
        $data = $this->main_model->find($id);

        if(is_null($data)){
            return Response()->json([
                "message" => "Not Found!"
            ], 404);
        }

        try {
            $success = $this->main_model
                ->where('ID', '=', $id)
                ->delete();
        } catch (\Illuminate\Database\QueryException $ex) {
            return Response()->json(
                ['message' => 'Data is being used!'
            ], 500);
        }

        if(!$success) {
            return Response()->json(
                ['message' => 'Error Deleting!'
            ], 500);
        }

        return Response()->json(
            ['message' => 'Deleted Successfully!'
        ], 200);

    }
}
