<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'session';

    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userID', 'name', 'description', 'start', 'duration',
    ];

    public static function rules() {
        return [
            'userID'           => 'required|exists:user,id',
            'name'             => 'required',
            'description'      => 'required',
            'start'            => 'required',
            'duration'         => 'required|numeric|min:1'
        ];
    }
}
