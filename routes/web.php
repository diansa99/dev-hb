<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('auth/login',['uses' => 'AuthController@authenticate']);
$router->post('auth/register',['uses' => 'AuthController@register']);

$router->get('session',  ['uses' => 'SessionController@index']);
$router->get('session/show/{id}',  ['uses' => 'SessionController@show']);

$router->group(['middleware' => 'jwt.auth'], function() use ($router) {
	$router->post('session/store',  ['uses' => 'SessionController@store']);
	$router->put('session/{id}/update',  ['uses' => 'SessionController@update']);
	$router->delete('session/{id}/destroy', ['uses' => 'SessionController@destroy']);
});

